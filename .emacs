
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-PDF-from-DVI nil)
 '(TeX-view-program-selection
   '(((output-dvi has-no-display-manager)
      "dvi2tty")
     ((output-dvi style-pstricks)
      "dvips and gv")
     (output-dvi "xdvi")
     (output-pdf "Atril")
     (output-html "xdg-open")))
 '(flycheck-grammalecte-enabled-modes '(org-mode text-mode mail-mode latex-mode lisp-mode))
 '(flycheck-grammalecte-report-spellcheck t)
 '(initial-major-mode 'text-mode)
 '(initial-scratch-message "#### SCRATCH ####

")
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("marmalade" . "https://marmalade-repo.org/packages/")
     ("melpa" . "https://melpa.org/packages/")))
 '(package-selected-packages
   '(vue-mode adoc-mode magit highlight-indent-guides yaml-mode caml markdown-mode poly-markdown polymode poly-R ess rust-mode flycheck-grammalecte flycheck tuareg auctex))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; arduino-mode
(require 'cl)
(autoload 'arduino-mode "arduino-mode" "Arduino editing mode." t)
(add-to-list 'auto-mode-alist '("\.ino$" . arduino-mode))

;; Flycheck (Correcteur de grammaire)
(with-eval-after-load 'flycheck
  (flycheck-grammalecte-setup))

;; rust-mode
(require 'rust-mode)

;; json-beautifier
(defun beautify-json ()
  (interactive)
  (let ((b (if mark-active (min (point) (mark)) (point-min)))
        (e (if mark-active (max (point) (mark)) (point-max))))
    (shell-command-on-region b e
     "python -mjson.tool" (current-buffer) t)))

;; avoid symlink creation with tmp files
(setq create-lockfiles nil)

