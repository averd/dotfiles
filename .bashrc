#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

git_branch () {
    local TMP=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    if [[ ! -z $TMP ]]; then
	echo " ($TMP)"
    fi    
}

cur_dir () {
    local TMP=$(pwd)
    if [ "${TMP:0:15}" = '/home/alexandre' ]; then
	TMP="~${TMP:15}"
    fi
    if [ ${#TMP} -gt 50 ]; then
	TMP="${TMP: -50}"
    fi
    echo "$TMP"
}


GREEN="$(tput setaf 40)"
ORANGE="$(tput setaf 214)"
BLUE="$(tput setaf 45)"
RED="$(tput setaf 196)"
RESET="$(tput sgr0)"
PS1='\[${GREEN}\]\u@\h\[${RESET}\] \[${BLUE}\]$(cur_dir)\[${RESET}\]\[${ORANGE}\]$(git_branch)\[${RESET}\] \[${RED}\]\$\[${RESET}\] '

if [[ ":$PATH:" != *":./bin:"* ]]; then 
    export PATH="${PATH}:./bin:/home/alexandre/Téléchargements/labtainer/trunk/scripts/designer/bin"
    export LABTAINER_DIR=/trunk
fi

alias ls='ls --color=auto'
alias tlmgr='/usr/share/texmf-dist/scripts/texlive/tlmgr.pl --usermode'
alias dotgit='git --git-dir=$HOME/.dotgit/ --work-tree=$HOME'
alias vi="emacs -nw"

PATH=/opt/anaconda/bin:$PATH
